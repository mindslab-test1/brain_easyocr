from easyocr.easyocr import Reader
import os
import cv2
import numpy as np
import time

if __name__ == '__main__':
    filepath = "ibk-sample/ctw_table.jpg"
    reader_main = Reader(['en'], gpu=True, model_storage_directory="./pretrained")
    reader_kor = Reader(['ko'], gpu=True, model_storage_directory="./pretrained",
                        detector=False)
    img_cv_grey, horizontal_list, free_list = reader_main.detect_text(filepath, text_threshold=0.9, link_threshold=0.9)

    tic = time.time()
    bounds = reader_main.recognize_text(img_cv_grey, horizontal_list, free_list, decoder="wordbeamsearch")

    idx_list = []
    horizontal_korean_candidate = []
    free_korean_candidate = []
    free_list_len = len(free_list)
    for idx, bound in enumerate(bounds):
        if bound[2] < 0.1:
            idx_list.append(idx)
            if free_list_len > idx:
                free_korean_candidate.append(free_list[idx])
            else:
                horizontal_korean_candidate.append(horizontal_list[idx - free_list_len])

    bounds_korean = reader_main.recognize_text(img_cv_grey,
                                               horizontal_korean_candidate, free_korean_candidate,
                                               decoder="greedy")

    for kor_idx, new_bound in enumerate(bounds_korean):
        bounds[idx_list[kor_idx]] = new_bound

    toc = time.time()

    image = cv2.imread(filepath, cv2.IMREAD_COLOR)
    color = (255, 0, 0)
    print("processing time: {:.05f}".format(toc - tic))
    filename = "result_wbsearch.txt"
    with open(filename, "w") as f:
        for bound in bounds:
            bbox = bound[0]
            pts = np.array(bbox, np.int32)
            pts = pts.reshape((-1, 1, 2))
            image = cv2.polylines(image, [pts], True, color, thickness=5)
            f.write("{:s}\t{:.05f}\n".format(bound[1], bound[2]))

    cv2.imwrite("example_image_save_07.jpg", image)
