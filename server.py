from __future__ import print_function, division
import argparse
import os
import grpc
import time
from concurrent import futures
from proto.brain_ocr_pb2_grpc import (
    add_OCRServiceServicer_to_server,
    OCRServiceServicer,
)
from proto.brain_ocr_pb2 import TextBoxes, TextBox, Point
from easyocr.easyocr import Reader


_ONE_DAY_IN_SECONDS = 60 * 60 * 24
_CHUNK_SIZE = 1024*1024

class OCRServiceServicerImpl(OCRServiceServicer):
    def __init__(self, gpu_id):
        super().__init__()
        self.gpu_id = gpu_id
        os.environ['CUDA_VISIBLE_DEVICES'] = str(self.gpu_id)  # TODO

        self.reader_main = Reader(['en'], gpu=True, model_storage_directory="./pretrained")
        self.reader_kor = Reader(['ko'], gpu=True, model_storage_directory="./pretrained",
                            detector=False)

    def Determinate(self, document_iterator, context):  # mainly called from client
        try:
            image_bytes = self._get_document_iterator(document_iterator)
            img_cv_grey, horizontal_list, free_list = self.reader_main.detect_text(image_bytes,
                                                                                   link_threshold=0.1, text_threshold=0.9)
            bounds = self.reader_main.recognize_text(img_cv_grey, horizontal_list, free_list, decoder="wordbeamsearch")

            idx_list, horizontal_korean_candidate, free_korean_candidate = \
                self._filter_bbox(bounds, horizontal_list, free_list)

            bounds_korean = self.reader_kor.recognize_text(img_cv_grey,
                                                       horizontal_korean_candidate, free_korean_candidate,
                                                       decoder="greedy")

            for kor_idx, new_bound in enumerate(bounds_korean):
                bounds[idx_list[kor_idx]] = new_bound

            bboxes = [bound[0] for bound in bounds]
            text_list = [bound[1] for bound in bounds]
            textbox_iterator = self._set_textboxes_iterator(bboxes, text_list)
            return textbox_iterator

        except Exception as e:  # logging when an exception occurs
            context.set_code(grpc.StatusCode.UNKNOWN)
            context.set_details(str(e))

    @staticmethod
    def _get_document_iterator(iterator):
        image = bytearray()
        for document in iterator:
            if document.image is not None:
                image.extend(document.image)
        image = bytes(image)
        return image


    @staticmethod
    def _set_textboxes_iterator(bboxes, text_list):
        # no output for image field
        bbox_list = []
        for _bbox, _text in zip(bboxes, text_list):
            textbox = []
            for point in _bbox:
                textbox.append(Point(x=int(point[0]), y=int(point[1])))
            bbox_list.append(TextBox(bbox=textbox, text=_text))

        yield TextBoxes(textbox=bbox_list)

    @staticmethod
    def _filter_bbox(bounds, horizontal_list, free_list):
        idx_list = []
        horizontal_korean_candidate = []
        free_korean_candidate = []
        free_list_len = len(free_list)
        for idx, bound in enumerate(bounds):
            if bound[2] < 0.07 and len(bound[1]) <= 5:
                idx_list.append(idx)
                if free_list_len > idx:
                    free_korean_candidate.append(free_list[idx])
                else:
                    horizontal_korean_candidate.append(horizontal_list[idx - free_list_len])

        return idx_list, horizontal_korean_candidate, free_korean_candidate


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--log_level", type=str, default="INFO", help="logger level")
    parser.add_argument("--gpuid", type=str, default="0", help="gpu id")
    parser.add_argument("--port", type=int, default=30001, help="main gRPC server port")

    args = parser.parse_args()

    server = grpc.server(
        futures.ThreadPoolExecutor(max_workers=1),
    )
    ocr_servicer = OCRServiceServicerImpl(args.gpuid)
    add_OCRServiceServicer_to_server(ocr_servicer, server)
    server.add_insecure_port("[::]:{}".format(args.port))
    print("Server Started >> : {}".format(args.port))
    server.start()

    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    main()
