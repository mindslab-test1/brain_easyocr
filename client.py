import argparse
import os
import grpc
import json

from proto.brain_ocr_pb2 import TextBoxes, TextBox, Point
from proto.brain_ocr_pb2_grpc import OCRServiceStub

_CHUNK_SIZE = 1024*1024

class OCRClassifierClient(object):
    def __init__(self, remote):
        channel = grpc.insecure_channel(remote)
        self.stub = OCRServiceStub(channel)

    def determinate(self, img_binary):
        iterator = self._get_document_iterator(img_binary)

        result_iterator = self.stub.Determinate(iterator)
        _, bbox_list = self._extract_data(result_iterator)
        return bbox_list

    @staticmethod
    def _get_bbox_list(textboxes):
        bboxes = []
        for textbox in textboxes:
            points = []
            for bbox in textbox.bbox:
                point = [bbox.x, bbox.y]
                points.append(point)

            text = textbox.text

            bboxes.append([points, text])
        return bboxes

    def _extract_data(self, iterator):
        image_binary = bytearray()
        bbox_packed = None
        for chunk in iterator:
            if chunk.image is not None:
                image_binary.extend(chunk.image)
            if chunk.textbox is not None:
                bbox_packed = chunk.textbox
        image_binary = bytes(image_binary)
        bbox_list = self._get_bbox_list(bbox_packed)
        return image_binary, bbox_list

    @staticmethod
    def _get_document_iterator(image_binary):
        for i in range(0, len(image_binary), _CHUNK_SIZE):
            yield TextBoxes(image=image_binary[i:i + _CHUNK_SIZE])


def _vis_bbox(image_path, bboxes):
    import cv2
    from PIL import Image, ImageDraw, ImageFont
    import numpy as np

    plot_path = os.path.splitext(image_path)[0] + "_plotted.jpg"

    img = cv2.imread(image_path, cv2.IMREAD_COLOR)

    text_color = (255, 0, 0)
    bbox_color = (255, 0, 255)
    fontpath = "./gulim.ttf"
    font = ImageFont.truetype(fontpath, 20)

    for pnts, text in bboxes:
        cv2.rectangle(img, tuple(pnts[0]), tuple(pnts[2]), bbox_color, 2)

    img_pil = Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    draw = ImageDraw.Draw(img_pil)

    # TODO: unify with cv2 library
    for pnts, text in bboxes:
        org = [np.max([x[0] for x in pnts]), np.min([x[1] for x in pnts])]
        draw.text(tuple(org), text, font=font, fill=text_color)

    plot_output = cv2.cvtColor(np.array(img_pil), cv2.COLOR_RGB2BGR)
    cv2.imwrite(plot_path, plot_output)
    return plot_output

def save_csv(bboxes, image_path):
    csv_path = os.path.splitext(image_path)[0] + ".csv"
    with open(csv_path, "w") as f:
        for points, text in bboxes:
            coords_strlist = []
            for point in points:
                coords_strlist.append("{:d},{:d}".format(point[0], point[1]))
            coords_str = ",".join(coords_strlist)
            f.write(",".join([coords_str, text]) + "\n")

    return csv_path

def save_json(bboxes, image_path):
    json_path = os.path.splitext(image_path)[0] + ".json"
    result_dict = dict()
    for idx, (points, text) in enumerate(bboxes):
        object_dict = dict()
        object_dict["bbox"] = points
        object_dict["text"] = text
        result_dict["{:03d}".format(idx)] = object_dict

    with open(json_path, 'w', encoding='utf-8') as f:
        json.dump(result_dict, f, ensure_ascii=False, indent=4)

    return json_path

def get_ocr_result(client, image_path, visualize_bbox, output_path):
    print("Processing for: {:s}".format(image_path))
    with open(image_path, "rb") as rf:
        img_binary = rf.read()

    output = client.determinate(img_binary)
    if visualize_bbox:
        _ = _vis_bbox(image_path, output)

    _ = save_csv(output, image_path)
    _ = save_json(output, image_path)
    for bbox, text in output:
        print(bbox, text)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='OCR Recognition client')
    parser.add_argument('-r', '--remote',
                        nargs='?',
                        dest='remote',
                        help='grpc: ip:port',
                        type=str,
                        default='localhost:5022')

    parser.add_argument('-i', '--input',
                        nargs='?',
                        dest='input',
                        help='Image path',
                        type=str,
                        required=True)

    parser.add_argument('-o', '--output',
                        nargs='?',
                        dest='output',
                        help='Text output path',
                        type=str,
                        default='./result.jpg')

    parser.add_argument('--visualize_bbox',
                        action='store_true',
                        help="whether to get the image with bbox marked")

    args = parser.parse_args()

    client = OCRClassifierClient(args.remote)
    if os.path.isdir(args.input):
        image_list = [os.path.join(args.input, x) for x in os.listdir(args.input)
                      if os.path.splitext(x)[-1] in [".jpg", ".png", ".jpeg", ".JPG", ".JPEG", ".PNG"] and
                      not "_plotted.jpg" in x]
        for image_path in image_list:
            get_ocr_result(client, image_path, args.visualize_bbox, args.output)

    else:
        get_ocr_result(client, args.input, args.visualize_bbox, args.output)




