FROM docker.maum.ai:443/brain/vision:cu101

# Configure apt and install packages
RUN apt-get update -y && \
    apt-get install -y \
    libglib2.0-0 \
    libsm6 \
    libxext6 \
    libxrender-dev \
    libgl1-mesa-dev \
    git \
    # cleanup
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/li

COPY . /app
#COPY requirements.txt /app/
WORKDIR /app

RUN pip install -r requirements.txt
RUN pip install grpcio
RUN pip install grpcio-tools

EXPOSE 65300